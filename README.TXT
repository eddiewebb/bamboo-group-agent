Bamboo Group Agent Plugin

This plugin provides the ability for Local or Remote agents to be reserved for particular build plans.

THe logical idea being that a group, team, or application might require extraordinary controls, libraries, builders, etc to warrant a dedicated build agent.

Once an agent is marked as a "Group Agent"; only plans that specify that Agent mayb build there. Any regular agents and plans remain unaffected, and will be delegated to the first available non-GA agent.



===== LICENSING ======
  Copyright 2012 Edward A. Webb 

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.



===== Contributing and Building ======

This plugin was built with Atlassian's SDK.

Here are the SDK commands you'll use immediately:

* atlas-run   -- installs this plugin into Bamboo and starts it on http://<machinename>:6990/bamboo
* atlas-debug -- same as atlas-run, but allows a debugger to attach at port 5005
* atlas-cli   -- after atlas-run or atlas-debug, opens a Maven command line window:
                 - 'pi' reinstalls the plugin into the running Bamboo instance
* atlas-help  -- prints description for all commands in the SDK

Full documentation is always available at:

https://developer.atlassian.com/display/DOCS/Developing+with+the+Atlassian+Plugin+SDK
