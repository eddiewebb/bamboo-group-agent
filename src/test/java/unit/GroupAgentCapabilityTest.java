package unit;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;

import java.util.Map;

import org.hamcrest.core.IsNot;
import org.junit.Test;

import com.atlassian.bamboo.v2.build.agent.capability.Capability;
import com.edwardawebb.bamboo.groupagent.capabilities.GroupAgentCapability;
public class GroupAgentCapabilityTest {
	
	/**
	 * Documentaion on this class's interface isn't great, but through trial and error I learned some things, summarizxed by the tests below
	 * 
	 * 
	 */

	@Test
	public void allKeysAndValuesReturnTheSameKey(){
		GroupAgentCapability capability = new GroupAgentCapability();		

		assertThat(capability.getCapabilityTypeKey(),is(GroupAgentCapability.CAPABILITY_TYPE_KEY));
		assertThat(capability.getCapabilityTypeLabel(),is(GroupAgentCapability.CAPABILITY_TYPE_KEY));
		assertThat(capability.getLabel(GroupAgentCapability.CAPABILITY_TYPE_KEY),is(GroupAgentCapability.CAPABILITY_TYPE_KEY));
		assertThat(capability.getNewKeyFromLabel(GroupAgentCapability.CAPABILITY_TYPE_KEY, GroupAgentCapability.CAPABILITY_TYPE_KEY),is(GroupAgentCapability.CAPABILITY_TYPE_KEY));
		assertThat(capability.getValueDescriptionKey(GroupAgentCapability.CAPABILITY_TYPE_KEY, GroupAgentCapability.CAPABILITY_TYPE_KEY),is(GroupAgentCapability.CAPABILITY_TYPE_KEY));
		
	}
	
	@Test
	public void capabilityReturnsACapabilityWhenPassedAnActionMap(){
		GroupAgentCapability groupAgentCapability = new GroupAgentCapability();			
		
		Capability returned = groupAgentCapability.getCapability(mock(Map.class));
		
		assertThat(returned,notNullValue());
		assertThat(returned.getKey(),is(GroupAgentCapability.CAPABILITY_TYPE_KEY));
	}
	
	
}
