package unit;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import org.junit.Before;
import org.junit.Test;
import org.springframework.test.annotation.Repeat;

import com.atlassian.bamboo.build.BuildDefinition;
import com.atlassian.bamboo.plan.Plan;
import com.atlassian.bamboo.plan.PlanManager;
import com.atlassian.bamboo.v2.build.BuildContext;
import com.atlassian.bamboo.v2.build.CommonContext;
import com.atlassian.bamboo.v2.build.agent.BuildAgent;
import com.atlassian.bamboo.v2.build.agent.LocalAgentDefinitionImpl;
import com.atlassian.bamboo.v2.build.agent.RemoteAgentDefinitionImpl;
import com.atlassian.bamboo.v2.build.agent.capability.CapabilityImpl;
import com.atlassian.bamboo.v2.build.agent.capability.CapabilitySet;
import com.atlassian.bamboo.v2.build.agent.capability.CapabilitySetImpl;
import com.atlassian.bamboo.v2.build.agent.capability.RequirementSet;
import com.edwardawebb.bamboo.groupagent.capabilities.GroupAgentCapability;
import com.edwardawebb.bamboo.groupagent.config.AgentPlanConfigurationPlugin;
import com.edwardawebb.bamboo.groupagent.filter.GroupAgentFilter;


public class GroupAgentFilterTest {

	private static final long TOP_PLAN_ID = 1112;
	public static String TOP_PLAN_KEY ="A1-A1-JOB1" ;
	public static String AGENT_A_GA ="AgentA" ;
	public static String AGENT_B_GA ="AgentB" ;
	public static String AGENT_FREE ="Default" ;
	
	
	HashMap<String, String> config;
	GroupAgentFilter filter;
	BuildContext context;
	Collection<BuildAgent> agents;
	RequirementSet requirements;
	BuildAgent agentA;
	BuildAgent agentB;
	BuildAgent defAgent;
	
	
	@Before
	public  void setup(){

		 //setup mock plan to reutnr a config that each test below can override
		BuildDefinition bd = mock(BuildDefinition.class);
		 config = mock(HashMap.class);
		when(bd.getCustomConfiguration()).thenReturn(config);	 
		Plan plan = mock(Plan.class);
		when(plan.getBuildDefinition()).thenReturn(bd);
		
		// this is an example of a bad mock, and shows smelly code.
		// The cast in the filter class is an admitted hack, but
		// I hope atlassian will expand their "dedcated agent" concept before I refactor this for a clean bamboo 5.x impl
		context = mock(BuildContext.class, withSettings().defaultAnswer(RETURNS_DEEP_STUBS).extraInterfaces(CommonContext.class));
		when(context.getParentBuildContext().getPlanId()).thenReturn(TOP_PLAN_ID);
		
		PlanManager pmMock = mock(PlanManager.class);
		when(pmMock.getPlanById(TOP_PLAN_ID)).thenReturn(plan);
		
		//give our mock planmanager to our testable filter
		filter = new GroupAgentFilter();
		filter.setPlanManager(pmMock);

		CapabilitySet csWith = new CapabilitySetImpl();
		CapabilitySet csWithout = new CapabilitySetImpl();
		csWith.addCapability(new CapabilityImpl(GroupAgentCapability.CAPABILITY_TYPE_KEY,"value"));
		LocalAgentDefinitionImpl agentDefinitionWithGroupAgent = mock(LocalAgentDefinitionImpl.class);
		when(agentDefinitionWithGroupAgent.getCapabilitySet()).thenReturn(csWith);
		LocalAgentDefinitionImpl agentDefinitionWithOutGroupAgent = mock(LocalAgentDefinitionImpl.class);
		when(agentDefinitionWithOutGroupAgent.getCapabilitySet()).thenReturn(csWithout);
		
		 //build agent pool with 2 GAs, and one free agent
		 agents = new ArrayList<BuildAgent>();
		 agentA =mock(BuildAgent.class);
		 when(agentA.getDefinition()).thenReturn(agentDefinitionWithGroupAgent);
		 when(agentA.getName()).thenReturn(AGENT_A_GA);
		 agents.add(agentA);
		 agentB =mock(BuildAgent.class);
		 when(agentB.getDefinition()).thenReturn(agentDefinitionWithGroupAgent);
		 when(agentB.getName()).thenReturn(AGENT_B_GA);
		 agents.add(agentB);
		 defAgent =mock(BuildAgent.class);
		 when(defAgent.getDefinition()).thenReturn(agentDefinitionWithOutGroupAgent);
		 when(defAgent.getName()).thenReturn(AGENT_FREE);
		 agents.add(defAgent);
		
		 


	}
	

	@Test @Repeat(5)
	public void whenAPlanSpecifiesAnAgentOnlyThatAgentIsUsed(){
		when(config.get(AgentPlanConfigurationPlugin.GROUP_AGENT_REQUIRE_KEY)).thenReturn("true");
		when(config.get(AgentPlanConfigurationPlugin.GROUP_AGENT_LIST_KEY)).thenReturn(AGENT_A_GA);
		
		
		Collection<BuildAgent> results = filter.filter(context, agents, requirements);
		
		assertThat(results.size(),is(1));
		assertThat(results.contains(agentA),is(true));	
		assertThat(results.contains(agentB),is(false));		
	}
	
	@Test @Repeat(5)
	public void whenAListOfSpacelessPlansAreSpecifiesOnlyThoseAgentsAreUsed(){
		when(config.get(AgentPlanConfigurationPlugin.GROUP_AGENT_REQUIRE_KEY)).thenReturn("true");
		when(config.get(AgentPlanConfigurationPlugin.GROUP_AGENT_LIST_KEY)).thenReturn(AGENT_A_GA + "," + AGENT_B_GA);		
		
		Collection<BuildAgent> results = filter.filter(context, agents, requirements);
		
		assertThat(results.size(),is(2));
		assertThat(results.contains(agentA),is(true));	
		assertThat(results.contains(agentB),is(true));		
	}

	
	@Test @Repeat(5)
	public void whenAListOfSpacedPlansAreSpecifiesOnlyThoseAgentsAreUsed(){
		when(config.get(AgentPlanConfigurationPlugin.GROUP_AGENT_REQUIRE_KEY)).thenReturn("true");
		when(config.get(AgentPlanConfigurationPlugin.GROUP_AGENT_LIST_KEY)).thenReturn(AGENT_A_GA + "  ,    " + AGENT_B_GA);		
		
		Collection<BuildAgent> results = filter.filter(context, agents, requirements);
		
		assertThat(results.size(),is(2));
		assertThat(results.contains(agentA),is(true));	
		assertThat(results.contains(agentB),is(true));		
	}

	@Test @Repeat(5)
	public void whenAPlanDoesNotSpecifyAgentsOnlyFreeAgentIsUsed(){
		when(config.containsKey(AgentPlanConfigurationPlugin.GROUP_AGENT_REQUIRE_KEY)).thenReturn(false);
		
		
		Collection<BuildAgent> results = filter.filter(context, agents, requirements);
		
		assertThat(results.size(),is(1));
		assertThat(results.contains(defAgent),is(true));		
	}
	
	
	@Test
	public void whenRemoteAgentsHaveCapabilityTheyAreCaught(){
		//override the local configs to be remote configs
		CapabilitySet csWith = new CapabilitySetImpl();
		CapabilitySet csWithout = new CapabilitySetImpl();
		csWith.addCapability(new CapabilityImpl(GroupAgentCapability.CAPABILITY_TYPE_KEY,"value"));
		RemoteAgentDefinitionImpl agentDefinitionWithGroupAgent = mock(RemoteAgentDefinitionImpl.class);
		when(agentDefinitionWithGroupAgent.getCapabilitySet()).thenReturn(csWith);
		RemoteAgentDefinitionImpl agentDefinitionWithOutGroupAgent = mock(RemoteAgentDefinitionImpl.class);
		when(agentDefinitionWithOutGroupAgent.getCapabilitySet()).thenReturn(csWithout);
	//resave to mocks
		 when(agentA.getDefinition()).thenReturn(agentDefinitionWithGroupAgent);
		 when(agentB.getDefinition()).thenReturn(agentDefinitionWithGroupAgent);
		 when(defAgent.getDefinition()).thenReturn(agentDefinitionWithOutGroupAgent);
		 
		 
		 //when not rrequired by plan, only free is returned
		 when(config.containsKey(AgentPlanConfigurationPlugin.GROUP_AGENT_REQUIRE_KEY)).thenReturn(false);					
		Collection<BuildAgent> results = filter.filter(context, agents, requirements);
		assertThat(results.size(),is(1));
		assertThat(results.contains(defAgent),is(true));		


		//when is required both group agents returnes
		when(config.get(AgentPlanConfigurationPlugin.GROUP_AGENT_REQUIRE_KEY)).thenReturn("true");
		when(config.get(AgentPlanConfigurationPlugin.GROUP_AGENT_LIST_KEY)).thenReturn(AGENT_A_GA + "  ,    " + AGENT_B_GA);	
		 agents.add(defAgent);
		 agents.add(agentA);
		 agents.add(agentB);			
		results = filter.filter(context, agents, requirements);		
		assertThat(results.size(),is(2));
		assertThat(results.contains(agentA),is(true));	
		assertThat(results.contains(agentB),is(true));	
	}
 
	
}
