package com.edwardawebb.bamboo.groupagent.capabilities;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.bamboo.v2.build.agent.capability.AbstractCapabilityTypeModule;
import com.atlassian.bamboo.v2.build.agent.capability.Capability;
import com.atlassian.bamboo.v2.build.agent.capability.CapabilityImpl;
import com.atlassian.bamboo.v2.build.agent.capability.CapabilitySet;
import com.atlassian.bamboo.v2.build.agent.capability.CapabilitySetImpl;

public class GroupAgentCapability extends AbstractCapabilityTypeModule
        {
    private static final Logger log = LoggerFactory
            .getLogger(GroupAgentCapability.class);
    public static final String CAPABILITY_TYPE_KEY = "GroupAgent";
    private static final Object ALLOW_DEPLOYMENTS_KEY = "allowDeployments";
    public static final String VALUE_NONE = "None";
    public static final String VALUE_GROUP = "Group";
    public static final String VALUE_ALL = "All";
    public static final String VALUES[] = {VALUE_NONE, VALUE_GROUP, VALUE_ALL};

    public int getSortOrder()
    {
        return 1000;
    }

    /*
     * The next chunk of fields are used on display screens, and depend on a language properties file specified in the plugin xml.
     * Each of the methods should return "GroupAgent" to align with the following keys.
     *
     *
     * #Capability Screens
        agent.capability.type.GroupAgent.title=Group Agent Duty
        agent.capability.type.GroupAgent.description=The Group Agent capability denotes that this Agent should be reserved only for Plans that specify this Agent by name.
        agent.capability.type.GroupAgent.value=Is Active?
        agent.capability.type.GroupAgent.value.description=Whether this Agent is an Active Group Agent
        agent.capability.type.GroupAgent.key=Group Agent
        agent.capability.type.GroupAgent.key.description=
             *
     *
     * Uses as line item heading for an agent
     */

    @NotNull
    public String getCapabilityTypeKey()
    {
        return CAPABILITY_TYPE_KEY;
    }

    @NotNull
    public String getCapabilityTypeLabel()
    {
        return "GroupAgent";
    }

    @Override
    public String getLabel(String key)
    {
        return "GroupAgent" ;
    }

    @Nullable
    public String getExtraInfo(@NotNull String key)
    {
        return null;
    }

    @Nullable
    public String getValueDescriptionKey(@NotNull String key, @Nullable String value)
    {
        // return different descriptions for each value (None, Group, All)
        if (key.equalsIgnoreCase(CAPABILITY_TYPE_KEY)) {
            return "GroupAgent";
        } else {
            if (value.equalsIgnoreCase(VALUE_NONE)) return "No Deployment Projects are allowed to run on this Group Agent.";
            else if (value.equalsIgnoreCase(VALUE_GROUP)) return "Only Deployment Projects linked to Build Plans requiring this Group Agent are allowed.";
            else return "All Deployment Projects are allowed to run on this Group Agent.";
        }
    }

    public boolean isAllowRename()
    {
        return true;
    }

    @NotNull
    public String getNewKeyFromLabel(@NotNull String oldKey, @NotNull String label)
    {
        return label;
    }

    @NotNull
    public Map<String, String> validate(@NotNull Map<String, String[]> params)
    {
        Map<String, String> fieldErrors = new HashMap();
        return fieldErrors;
    }

    @Override
    public Capability getCapability(Map<String, String[]> params) {
        String[] allowDeploys = params.get(ALLOW_DEPLOYMENTS_KEY);
        String value = (allowDeploys == null ? VALUE_NONE : (VALUE_ALL.equalsIgnoreCase(allowDeploys[0]) ? VALUE_ALL : VALUE_GROUP));
        return new CapabilityImpl("GroupAgent", value);
    }
}
