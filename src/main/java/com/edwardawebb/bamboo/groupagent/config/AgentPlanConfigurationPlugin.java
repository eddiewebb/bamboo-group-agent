package com.edwardawebb.bamboo.groupagent.config;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.bamboo.plan.Plan;
import com.atlassian.bamboo.plan.TopLevelPlan;
import com.atlassian.bamboo.utils.error.ErrorCollection;
import com.atlassian.bamboo.v2.build.BaseBuildConfigurationAwarePlugin;
import com.atlassian.bamboo.v2.build.configuration.MiscellaneousBuildConfigurationPlugin;
import com.atlassian.bamboo.ww2.actions.build.admin.create.BuildConfiguration;

public class AgentPlanConfigurationPlugin extends
		BaseBuildConfigurationAwarePlugin implements
		MiscellaneousBuildConfigurationPlugin {
	private static final Logger log = LoggerFactory
			.getLogger(AgentPlanConfigurationPlugin.class);
	public static String GROUP_AGENT_LIST_KEY = "custom.bamboo-group-agent.agents";
	public static String GROUP_AGENT_REQUIRE_KEY = "custom.bamboo-group-agent.specialAgentsFlag";
	public static String LOCK_DEPLOYMENTS_KEY = "custom.bamboo-group-agent.lockDeployments";

	@Override
	public boolean isApplicableTo(Plan plan) {
		//only top level Plans see this option.
		return plan instanceof TopLevelPlan;
	}

	@Override
	public boolean isConfigurationMissing(BuildConfiguration buildConfiguration) {
		// TODO Auto-generated method stub
		return super.isConfigurationMissing(buildConfiguration);
	}

	@Override
	public ErrorCollection validate(BuildConfiguration buildConfiguration) {
		ErrorCollection errorCollection = super.validate(buildConfiguration);

		if(buildConfiguration.getBoolean(GROUP_AGENT_REQUIRE_KEY) && StringUtils.isEmpty(buildConfiguration.getString(GROUP_AGENT_LIST_KEY))){
			errorCollection.addError(GROUP_AGENT_LIST_KEY, "If requiring Group Agents, specify a comma separated list of Agent Names");
		}

		return errorCollection;
	}
}
