/**
 *
 */
package com.edwardawebb.bamboo.groupagent.filter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.bamboo.buildqueue.properties.CapabilityProperties;
import com.atlassian.bamboo.deployments.execution.DeploymentContext;
import com.atlassian.bamboo.deployments.projects.service.DeploymentProjectService;
import com.atlassian.bamboo.deployments.projects.DeploymentProject;
import com.atlassian.bamboo.plan.Plan;
import com.atlassian.bamboo.plan.PlanManager;
import com.atlassian.bamboo.v2.build.BuildContext;
import com.atlassian.bamboo.v2.build.CommonContext;
import com.atlassian.bamboo.v2.build.agent.BuildAgent;
import com.atlassian.bamboo.v2.build.agent.BuildAgentRequirementFilter;
import com.atlassian.bamboo.v2.build.agent.PipelineDefinitionImpl;
import com.atlassian.bamboo.v2.build.agent.capability.Capability;
import com.atlassian.bamboo.v2.build.agent.capability.CapabilitySet;
import com.atlassian.bamboo.v2.build.agent.capability.MinimalRequirementSet;
import com.edwardawebb.bamboo.groupagent.capabilities.GroupAgentCapability;
import com.edwardawebb.bamboo.groupagent.config.AgentPlanConfigurationPlugin;

/**
 * @author eddie
 *
 */
public class GroupAgentFilter implements BuildAgentRequirementFilter {

	private static final Logger log = LoggerFactory
			.getLogger(GroupAgentFilter.class);


	private PlanManager planManager;
	private DeploymentProjectService deploymentProjectService;

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.atlassian.bamboo.v2.build.agent.BuildAgentRequirementFilter#filter
	 * (com.atlassian.bamboo.v2.build.BuildContext, java.util.Collection,
	 * com.atlassian.bamboo.v2.build.agent.capability.RequirementSet)
	 */
	@Override
	public Collection<BuildAgent> filter(CommonContext commonContext,
			Collection<BuildAgent> availableAgents, MinimalRequirementSet requirements) {
		// Group Agents must be known regardless of path ( to choose - or remove)
		Collection<BuildAgent> groupAgents = new ArrayList<BuildAgent>();
		Collection<BuildAgent> deployableForGroupGroupAgents = new ArrayList<BuildAgent>();
		Collection<BuildAgent> deployableForAllGroupAgents = new ArrayList<BuildAgent>();

		for (BuildAgent buildAgent : availableAgents) {
			PipelineDefinitionImpl pipelineDefinition = (PipelineDefinitionImpl) buildAgent.getDefinition();

			if (pipelineDefinition instanceof CapabilityProperties) {
				CapabilitySet capabilities = ((CapabilityProperties)pipelineDefinition).getCapabilitySet()  ;
				Capability capability = capabilities.getCapability(GroupAgentCapability.CAPABILITY_TYPE_KEY);

				if (null != capability) {
					log.info(buildAgent.getName()
							+ " is identified as a _group_ agent");
					groupAgents.add(buildAgent);

					String value = capability.getValue();
					log.info(buildAgent.getName() + " Allow Deployment capability value: " + value);
					if (value.equalsIgnoreCase(GroupAgentCapability.VALUE_GROUP)) {
						deployableForGroupGroupAgents.add(buildAgent);
					} else if (value.equalsIgnoreCase(GroupAgentCapability.VALUE_ALL)) {
						deployableForAllGroupAgents.add(buildAgent);
					}
				}
			} // can't say, wouldn't know
		}

		Boolean isDeployment = false;
		Plan plan;

		// check if context is for Build Plan or Deployment Project
		if (commonContext instanceof DeploymentContext) {
			isDeployment = true;

			// get from deployment context to linked build plan and use plan's group agent configuration
			DeploymentProject deploymentProject = deploymentProjectService.getDeploymentProjectForEnvironment(((DeploymentContext)commonContext).getEnvironmentId());
			log.info("Deployment Project Plan Key: " + deploymentProject.getPlanKey());
			plan = planManager.getPlanByKey(deploymentProject.getPlanKey());

			log.info("Deployment context, removing {} agents from pool.", groupAgents.size());

			// keep only deployable group agents (later in code will add them)
			availableAgents.removeAll(groupAgents);
		} else {
			plan = planManager.getPlanById(((BuildContext)commonContext).getParentBuildContext().getPlanId());
		}

		// get configuration from this (job) context's parent (plan)
		Map<String, String> customConfig = plan.getBuildDefinition()
				.getCustomConfiguration();
		if (log.isDebugEnabled()) {
			for (String key : customConfig.keySet()) {
				log.debug("Key: " + key);
				log.debug(customConfig.get(key).toString());
			}
		}
		String agentList = (String) customConfig.get(AgentPlanConfigurationPlugin.GROUP_AGENT_LIST_KEY);
		Boolean isGroupAgentRequired = Boolean.parseBoolean(customConfig.get(AgentPlanConfigurationPlugin.GROUP_AGENT_REQUIRE_KEY));
		Boolean isDeploymentLocked = Boolean.parseBoolean(customConfig.get(AgentPlanConfigurationPlugin.LOCK_DEPLOYMENTS_KEY));

		// deployment or build
		if (isDeployment) {
			// dealing with deployment project
			if (isGroupAgentRequired && isDeploymentLocked) {
				// this deployment is locked to same group agents as it's parent plan
				log.info("This Deployment Project is locked to it's parent Plan's _group_ agent(s)");

				// run through group agents names and select matching from deployableForGroupGroupAgents
				List<String> planAgents = new ArrayList<String>();
				for (String desiredAgent : agentList.split(",")) {
					planAgents.add(desiredAgent.toLowerCase().trim());
					log.info("Parent Plan: " + plan.getKey()  +  " has configured association with Agent: " + desiredAgent.trim());
				}

				availableAgents.clear();
				for (BuildAgent agent : deployableForGroupGroupAgents) {
					if (planAgents.contains(agent.getName().toLowerCase())) {
						availableAgents.add(agent);
						log.info("Agent: " + agent.getName() +  " was located and added to available agents.");
					}
				}
			} else {
				// this deployment is not locked to it's parent plan's group agents
				// or parent plan has no group agents configured at all
				// thus can run on any of "free for all" deployable agents
				log.info("Unlocked Deployment Project can run on {} 'free for all' _group_ agents", deployableForAllGroupAgents.size());
				availableAgents.addAll(deployableForAllGroupAgents);
			}
		} else {
			// this is a build plan
			if (!isGroupAgentRequired) {
				log.info("No group Agents configured this Plan, all Group Agents will be removed from pool");
				// regular plan, remove group agents from pool and return
				availableAgents.removeAll(groupAgents);
			} else {
				// special plan, pull specified agents and return
				log.info("Plan is linked to particular _group_ agent(s)");
				List<String> planAgents = new ArrayList<String>();
				for (String desiredAgent : agentList.split(",")) {
					planAgents.add(desiredAgent.toLowerCase().trim());
					log.info("Plan: " + plan.getKey()  +  " has configured association with Agent: " + desiredAgent.trim());
				}

				availableAgents.clear();
				for (BuildAgent agent : groupAgents) {
					if (planAgents.contains(agent.getName().toLowerCase())) {
						availableAgents.add(agent);
						log.info("Agent: " + agent.getName() +  " was located and added to available agents.");
					}
				}
			}
		}

		if (availableAgents.isEmpty()) {
			log.warn("This project is specifying an agent that was not found. Build will be queued until one is available.");
		}

		return availableAgents;
	}

	public void setPlanManager(PlanManager planManager) {
		this.planManager = planManager;
	}

	public void setDeploymentProjectService(DeploymentProjectService deploymentProjectService) {
		this.deploymentProjectService = deploymentProjectService;
	}

}
