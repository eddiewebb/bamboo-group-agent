[@ui.bambooSection titleKey="Group Agents"]
    [@ww.checkbox labelKey='This Plan requires a Group Agent'
                  name='custom.bamboo-group-agent.specialAgentsFlag'
                  toggle='true' /]

    [@ui.bambooSection dependsOn='custom.bamboo-group-agent.specialAgentsFlag' showOn='true']
        [@ww.textfield labelKey='Agents'
                       name='custom.bamboo-group-agent.agents'
                        /]
        [@ww.checkbox labelKey='agent.capability.type.GroupAgent.lockDeployments'
        			  name='custom.bamboo-group-agent.lockDeployments'
        			  toggle='true' /]
    [/@ui.bambooSection]
[/@ui.bambooSection]